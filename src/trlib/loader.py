'''
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
'''

import json
from typing import List
from .parser import parse, ParseError
from .dom import Dom, Session

#def load_file(file_name:str, validate:bool=False) -> List[Session]:

    # load the data file as json
    #with open(file_name) as infile:
        #data = json.load(infile)

    # validate the file with schema
    #if validate:
        #jsonschema.validate(instance=data,schema=schema)

    # transform the data in to DOM (data object model)


def loads(datastr:str, dom = Dom) -> Dom:
    return load_json(json.loads(datastr),dom)

def load_json(json_data:dict, dom = Dom) -> Dom:
    return parse(json_data,dom)
